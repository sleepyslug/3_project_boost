﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Rocket : MonoBehaviour
{    
    [SerializeField] float rcsThrust = 250f;
    [SerializeField] float mainThrust = 35f;
    [SerializeField] float levelLoadDelay = 1f;

    [SerializeField] AudioClip mainEngine;
    [SerializeField] AudioClip shipCrash;
    [SerializeField] AudioClip winLevel;

    [SerializeField] ParticleSystem mainEngineParticles;
    [SerializeField] ParticleSystem shipCrashParticles;
    [SerializeField] ParticleSystem winLevelParticles;


    Rigidbody rigidBody;
    AudioSource audioSource;
    bool collisionsDisabled = false;


    enum State { Alive, Dying, Transcending }
    State state = State.Alive;
    

    // Start is called before the first frame update
    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
        
    }

    // Update is called once per frame
    void Update()
    {        
        if (state == State.Alive)
        {
            RespondToRotateInput();
            RespondToThrustInput();
        }

        if (Debug.isDebugBuild)
        {
            RespondToDebugKeys();
        }
        
        
    }

    private void RespondToDebugKeys()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            LoadNextScene();
        }

        else if (Input.GetKeyDown(KeyCode.C))
        {
            collisionsDisabled = !collisionsDisabled;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (state != State.Alive || collisionsDisabled)   { return; } //ignore conditions when dead

        switch (collision.gameObject.tag)
        {
            case "Friendly":
                break;
            case "Finish":
                StartSuccessSequence();// paramaterize the time  
                break;                       // Invoke needs the name of the method as a string
            default:
                StartDeathSequence();
                break;
        }
    }

    private void StartDeathSequence()
    {
        audioSource.Stop();
        mainEngineParticles.Stop();
        audioSource.PlayOneShot(shipCrash);
        shipCrashParticles.Play();
        state = State.Dying;
        Invoke("LoadFromStart", levelLoadDelay);
    }

    private void StartSuccessSequence()
    {
        audioSource.Stop();
        mainEngineParticles.Stop();
        audioSource.PlayOneShot(winLevel);
        winLevelParticles.Play();
        state = State.Transcending;
        Invoke("LoadNextScene", levelLoadDelay);
    }

    private void LoadFromStart()
    {
        SceneManager.LoadScene(0);        
    }

    private void LoadNextScene()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        int nextSceneIndex = currentSceneIndex + 1;
        //better solution
        if (nextSceneIndex == SceneManager.sceneCountInBuildSettings)
        {
            nextSceneIndex = 0;
        }


        //my solution
        //if (currentSceneIndex < SceneManager.sceneCountInBuildSettings - 1)
        //{
        //    nextSceneIndex = currentSceneIndex += 1;
        //}
        //else
        //{
        //    nextSceneIndex = 0;
        //}
        SceneManager.LoadScene(nextSceneIndex);
    }

    private void RespondToThrustInput()
    {
                
        if (Input.GetKey(KeyCode.Space))
        {
            ApplyThrust();

        }
        else
        {
            audioSource.Stop();
            mainEngineParticles.Stop();
        }
    }

    private void ApplyThrust()
    {
        rigidBody.AddRelativeForce(Vector3.up * mainThrust * Time.deltaTime);       

        if (!audioSource.isPlaying)
        {
            audioSource.PlayOneShot(mainEngine);
        }
        mainEngineParticles.Play();
    }

    private void RespondToRotateInput()
    {

        rigidBody.freezeRotation = true;

        float rotationThisFrame = rcsThrust * Time.deltaTime;

        if (Input.GetKey(KeyCode.A))
        {
            transform.Rotate(Vector3.forward * rotationThisFrame);
        }
        
        else if (Input.GetKey(KeyCode.D))
        {
            transform.Rotate(Vector3.back * rotationThisFrame);
        }

        rigidBody.freezeRotation = true;
    }
        
}
